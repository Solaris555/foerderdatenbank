<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnConsulter extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fn_consulter', function (Blueprint $table) {
            $table->increments('fn_consulter_id');
            $table->integer('fn_consulter_level_id');
            $table->integer('fn_politic_areas_id');
            $table->integer('fn_countries_id');
            $table->string('admin_name');
            $table->string('short');
            $table->string('institution');
            $table->string('institution_info');
            $table->string('director_name');

            $table->integer('generaldirection');
            $table->string('postal_street');
            $table->string('postal_postcode');
            $table->string('postal_city');
            $table->string('street');
            $table->string('postcode');
            $table->string('city');
            $table->string('region');
            $table->string('country');
            $table->string('email');
            $table->string('website');
            $table->string('website_description');
            $table->string('phone');
            $table->string('fax');
            $table->longtext('image');
            $table->string('phone_consulter');
            $table->string('fax_consulter');
            $table->string('email_consulter');
            $table->string('extrainfo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('fn_consulter');
    }

}
