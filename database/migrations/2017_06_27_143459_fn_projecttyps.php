<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnProjecttyps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_projecttyps', function (Blueprint $table) {
            $table->increments('fn_projecttyps_id');
            $table->string('de_name');
            $table->string('de_description');
            $table->string('en_name');
            $table->string('en_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_projecttyps');
    }
}

