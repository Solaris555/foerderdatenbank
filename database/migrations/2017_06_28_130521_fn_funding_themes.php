<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnFundingThemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_funding_themes', function (Blueprint $table) {
            $table->increments('fn_funding_themes_id');
            $table->integer('fn_funding_themes_top_id');
            $table->longtext('image');
            $table->string('de_name');
            $table->longtext('de_description');
            $table->string('en_name');
            $table->longtext('en_description');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_funding_themes');
    }
}
