<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnCountriesTop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_countries_top', function (Blueprint $table) {
            $table->increments('fn_countries_top_id');
            $table->string('kurz');
            $table->string('de_name');
            $table->string('eu_member');
            $table->string('eu_candidate');
            $table->string('efta');
            $table->string('euromed');
            $table->string('eec');
            $table->string('see');
            $table->string('latam');
            $table->string('akp');
            $table->longtext('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_countries_top');
    }
}
