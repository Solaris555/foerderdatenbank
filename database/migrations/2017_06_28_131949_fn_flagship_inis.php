<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnFlagshipInis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_flagship_inis', function (Blueprint $table) {
            $table->increments('fn_flagship_inis_id');
            $table->string('de_name');
            $table->string('de_description');
            $table->string('en_name');
            $table->string('en_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_flagship_inis');
    }
}
