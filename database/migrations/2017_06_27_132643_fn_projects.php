<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_projects', function (Blueprint $table) {
            $table->increments('fn_projects_id');
            $table->integer('fn_projecttyps_id');
            $table->string('name_de');
            $table->double('proj_budget_min', 15, 0);
            $table->double('proj_budget_max', 15, 0);
            $table->date('proj_phase1_opened');
            $table->date('proj_phase1_deadline');
            $table->date('proj_phase1_check_formal');
            $table->date('proj_phase1_decision');
            $table->date('proj_phase1_decision_info');
            $table->date('proj_phase1_agreement');
            $table->date('proj_phase1_start');
            $table->date('proj_phase1_end');
            $table->date('proj_phase2_opened');
            $table->date('proj_phase2_deadline');
            $table->date('proj_phase2_check_formal');
            $table->date('proj_phase2_decision');
            $table->date('proj_phase2_decision_info');
            $table->date('proj_phase2_agreement');
            $table->date('proj_phase2_start');
            $table->date('proj_phase2_end');
            $table->date('proj_phase3_opened');
            $table->date('proj_phase3_deadline');
            $table->date('proj_phase3_check_formal');
            $table->date('proj_phase3_decision');
            $table->date('proj_phase3_decision_info');
            $table->date('proj_phase3_agreement');
            $table->date('proj_phase3_start');
            $table->date('proj_phase3_end');
            $table->date('proj_phase4_opened');
            $table->date('proj_phase4_deadline');
            $table->date('proj_phase4_check_formal');
            $table->date('proj_phase4_decision');
            $table->date('proj_phase4_decision_info');
            $table->date('proj_phase4_agreement');
            $table->date('proj_phase4_start');
            $table->date('proj_phase4_end');
            $table->date('proj_phase5_opened');
            $table->date('proj_phase5_deadline');
            $table->date('proj_phase5_check_formal');
            $table->date('proj_phase5_decision');
            $table->date('proj_phase5_decision_info');
            $table->date('proj_phase5_agreement');
            $table->date('proj_phase5_start');
            $table->date('proj_phase5_end');
            $table->date('proj_phase6_opened');
            $table->date('proj_phase6_deadline');
            $table->date('proj_phase6_check_formal');
            $table->date('proj_phase6_decision');
            $table->date('proj_phase6_decision_info');
            $table->date('proj_phase6_agreement');
            $table->date('proj_phase6_start');
            $table->date('proj_phase6_end');
	    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_projects');
    }
}
