<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnNuts3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_nuts3', function (Blueprint $table) {
            $table->increments('fn_nuts3_id');
            $table->integer('fn_nuts2_id');
            $table->string('short');
            $table->string('de_name');
            $table->longtext('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_nuts3');
    }
}
