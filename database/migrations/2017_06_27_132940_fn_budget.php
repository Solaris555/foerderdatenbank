<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnBudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_budget', function (Blueprint $table) {
            $table->increments('fn_budget_id');
            $table->double('fn_mff_areas_id');
            $table->string('prog_bud_mff_name');
            $table->string('prog_bud_link');
            $table->double('prog_bud_total', 15, 0);
            $table->string('prog_bud_action1_name');
            $table->double('prog_bud_action1_bud', 15, 0);
            $table->string('prog_bud_action2_name');
            $table->double('prog_bud_action2_bud', 15, 0);
            $table->string('prog_bud_action3_name');
            $table->double('prog_bud_action3_bud', 15, 0);
            $table->string('prog_bud_action4_name');;
            $table->double('prog_bud_action4_bud', 15, 0);
            $table->string('prog_bud_division1_name');
            $table->double('prog_bud_division1_bud', 15, 0);
            $table->string('prog_bud_division2_name');
            $table->double('prog_bud_division2_bud', 15, 0);
            $table->string('prog_bud_division3_name');
            $table->double('prog_bud_division3_bud', 15, 0);
            $table->string('prog_bud_division4_name');
            $table->double('prog_bud_division4_bud', 15, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_budget');
    }
}
