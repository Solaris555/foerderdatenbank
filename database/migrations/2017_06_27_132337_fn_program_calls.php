<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FnProgramCalls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fn_program_calls', function (Blueprint $table) {
	$table->increments('id');
        $table->string('prog_call_tend');
        $table->integer('prog_call_eu_period');
        $table->string('prog_call_struc_act');
        $table->string('prog_call_name_en');
        $table->string('prog_call_name_de');
        $table->string('image');
        $table->integer('fn_targetgroups_id');
        $table->enum('fn_funding_themes_id', ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52']);
      //  $table->integer('prog_call_funding_themes_id');
        $table->longtext('prog_call_measures');
       $table->enum('fn_countries_id', ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70', '71', '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82', '83', '84', '85', '86', '87', '88', '89', '90', '91', '92', '93', '94', '95', '96', '97', '98', '99', '100', '101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112', '113', '114', '115', '116', '117', '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138', '139', '140', '141', '142', '143', '144', '145', '146', '147', '148', '149', '150', '151', '152', '153', '154', '155', '156', '157', '158', '159', '160', '161', '162', '163', '164', '165', '166', '167', '168', '169', '170', '171', '172', '173', '174', '175', '176', '177', '178', '179', '180', '181', '182', '183', '184', '185', '186', '187', '188', '189', '190', '191', '192', '193', '194', '195', '196', '197', '198', '199', '200', '201', '202', '203', '204', '205', '206', '207', '208', '209', '210', '211', '212', '213', '214', '215', '216', '217', '218', '219', '220', '221', '222', '223', '224', '225', '226', '227', '228', '229', '230', '231']);
       //  $table->integer('countries_id');
        $table->longtext('prog_call_allow_country_add_info');
       $table->enum('fn_applicants_id', ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77']);
      //  $table->integer('allow_applicants_id');
        $table->longtext('prog_call_allow_applicants_add_info');
        $table->enum('fn_applicants_quali_id', ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17']);
       // $table->integer('allow_applicants_quali_id');
        $table->longtext('prog_call_reasons');
        $table->longtext('prog_changes_prephase');
        $table->longtext('prog_call_results');
        $table->longtext('prog_call_added_values');
        $table->longtext('prog_call_indicators');
        $table->date('prog_proj_first_start');
        $table->date('prog_proj_last_end');
        $table->integer('fn_budget_id');
        $table->string('prog_call_permanent_deadline');
        $table->integer('fn_priorities_id');
        $table->integer('fn_flagship_inis_id');
        $table->integer('fn_mff_areas_id');
        $table->integer('fn_mainstream_topics_id');
//        $table->integer('prog_call_sector_id');
//        $table->integer('prog_call_eu_gds_id');
//        $table->string('prog_call_gd_specarea');
//      $table->char('prog_call_gds_match');
//        $table->integer('prog_call_ea_id');
//        $table->string('prog_call_ea_specarea');
//      $table->char('prog_call_ea_match');
//        $table->integer('prog_call_nat_ministry_at_id');
//        $table->string('prog_call_nat_ministry_at__specarea');
//      $table->char('prog_call_nat_ministry_at_match');
//        $table->integer('prog_call_nat_ministry_de_id');
//        $table->string('prog_call_nat_ministry_de_specarea');
//      $table->char('prog_call_nat_ministry_de_match');
        $table->integer('prog_call_free_consultor');
        $table->string('prog_call_free_consultor_specarea');
        $table->integer('prog_num_calls_per_year');
        $table->integer('fn_projects_id');
        
        $table->longtext('prog-call_dllink');
        $table->date('prog_call_deadline_apply');
        $table->string('prog_link-ext_dates');
        $table->longtext('prog_linkname-addinfo');
        $table->string('prog_call_background');
//      $table->varchar('prog_call_metatag');
        $table->date('prog_call_publish_up');
        $table->date('prog_call_publish_down');
        $table->string('prog_call_published');
        $table->date('prog_call_created');
        $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fn_program_calls');
    }
}
